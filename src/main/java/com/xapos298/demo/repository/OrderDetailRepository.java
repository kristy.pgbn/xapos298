package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos298.demo.model.OrderDetail;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long>{
//	@Query(value = "SELECT * FROM order_detail a JOIN order_header b ON a.header_id = b.id WHERE b.id=?1", nativeQuery = true)
//	List<OrderDetail> findByHeaderId(Long id);
	
	public List<OrderDetail> findByHeaderId(long id);
}
