package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos298.demo.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{
	List<Product> findByisActive(Boolean active); 
	
	@Query(value = "SELECT * FROM product WHERE(lower(product_name) LIKE lower(concat('%', ?1, '%')))", nativeQuery = true)
	List<Product> searchByKey(String key);
}
