package com.xapos298.demo.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.xapos298.demo.model.Variants;
import com.xapos298.demo.repository.CategoryRepository;
import com.xapos298.demo.repository.VariantsRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiVariantController {

	@Autowired
	public VariantsRepository variantsRepository;

	@Autowired
	public CategoryRepository categoryRepository;

	@GetMapping("variants")
	public ResponseEntity<List<Variants>> getAllVariants() {
		try {

//			boolean status = new Variants().getCategory().getIsActive();
//			boolean status1 = true;
//			if (status == status1) {
			List<Variants> listVariants = this.variantsRepository.findByVarian(true, true);
			return new ResponseEntity<List<Variants>>(listVariants, HttpStatus.OK);
//			}

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
//		return null;
	}
	
	@GetMapping("variantsbycategory/{id}")
	public ResponseEntity<List<Variants>> getVariantByCategoryId(@PathVariable("id") Long id){
		try {
			List<Variants> variantsbycatid = this.variantsRepository.findByVarianCategoryId(true, true, id);
			return new ResponseEntity<List<Variants>>(variantsbycatid,HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("variants/add")
	public ResponseEntity<Object> saveVariants(@RequestBody Variants variants) {

		variants.setCreateBy("user1");
		variants.setCreateDate(new Date());

		Variants variantsData = this.variantsRepository.save(variants);

		if (variantsData.equals(variants)) {
			return new ResponseEntity<>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("variants/{id}")
	public ResponseEntity<List<Variants>> getCategoryById(@PathVariable("id") Long id) {
		try {
			Optional<Variants> variants = this.variantsRepository.findById(id);
			if (variants.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(variants, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Variants>>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("edit/variants/{id}")
	public ResponseEntity<Object> editVariantsEntity(@PathVariable("id") Long id, @RequestBody Variants variants) {
		Optional<Variants> variantsData = this.variantsRepository.findById(id);
//		Optional<Category> categorysData = this.categoryRepository.findById(id);
		if (variantsData.isPresent()) {
//			Category category = new Category();
			variants.setCategoryId(variantsData.get().getCategoryId());
			variants.setId(id);
			variants.setModifyBy("user1");
			variants.setModifyDate(new Date());
			variants.setCreateBy(variantsData.get().getCreateBy());
			variants.setCreateDate(variantsData.get().getCreateDate());
//			variants.getCategory().setIsActive(variantsData.get().getCategory().getIsActive());
			this.variantsRepository.save(variants);
			return new ResponseEntity<Object>("Update Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PutMapping("delete/variants/{id}")
	public ResponseEntity<Object> deleteVariants(@PathVariable("id") Long id) {
		Optional<Variants> variantsData = this.variantsRepository.findById(id);

		if (variantsData.isPresent()) {
			Variants variants = new Variants();
			variants.setId(id);
			variants.setActive(false);
			variants.setModifyBy("user1");
			variants.setModifyDate(new Date());
			variants.setInitial(variantsData.get().getInitial());
			variants.setCreateBy(variantsData.get().getCreateBy());
			variants.setCreateDate(variantsData.get().getCreateDate());
			variants.setName(variantsData.get().getName());
			variants.setCategoryId(variantsData.get().getCategoryId());
//			variants.getCategory().setIsActive(variantsData.get().getCategory().getIsActive());
			this.variantsRepository.save(variants);
			return new ResponseEntity<>("Deleted Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@GetMapping("search/variants/{key}")
	public ResponseEntity<List<Variants>> search(@PathVariable("key") String key){
		try {
			List<Variants> searchVariants = this.variantsRepository.searchByKey(key);
			return new ResponseEntity<List<Variants>>(searchVariants,HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

}
