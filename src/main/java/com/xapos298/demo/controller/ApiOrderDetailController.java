package com.xapos298.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos298.demo.model.OrderDetail;
import com.xapos298.demo.model.Variants;
import com.xapos298.demo.repository.OrderDetailRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiOrderDetailController {
	@Autowired
	public OrderDetailRepository orderDetailRepository;
	
	@PostMapping("orderdetail/add")
	public ResponseEntity<Object> saveOrderDetail(@RequestBody OrderDetail orderDetail){
		OrderDetail orderDetailData = this.orderDetailRepository.save(orderDetail);
		if(orderDetailData.equals(orderDetail)) {
			return new ResponseEntity<Object>("Save item success", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save failed", HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("orderdetail/list/{id}")
	public ResponseEntity<List<OrderDetail>> getAllOrderDetail(@PathVariable("id") Long id){
		try {
			List<OrderDetail> listOrderDetail = this.orderDetailRepository.findByHeaderId(id);
			return new ResponseEntity<List<OrderDetail>>(listOrderDetail, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
}
