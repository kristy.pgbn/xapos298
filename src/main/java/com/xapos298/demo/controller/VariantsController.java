package com.xapos298.demo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos298.demo.model.Category;
import com.xapos298.demo.model.Variants;
import com.xapos298.demo.repository.CategoryRepository;
import com.xapos298.demo.repository.VariantsRepository;

@Controller
@RequestMapping("/variants/")
public class VariantsController {
	
	@Autowired
	private VariantsRepository variantsRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@GetMapping("indexvarapi")
	public ModelAndView indexvarapi() {
		ModelAndView view = new ModelAndView("variants/indexvarapi");
		return view;
	}
	
	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("variants/index");
		List<Variants> listVariants = this.variantsRepository.findByActive(true); //menggunakan native query
		view.addObject("listVariants", listVariants);
		return view;
	}
	
	@GetMapping("addformvariants")
	public ModelAndView addformvariants() {
		ModelAndView view = new ModelAndView("variants/addformvariants");
		Variants variants = new Variants();
		view.addObject("variants", variants);
		//ambil category list
		List<Category> listCategory = this.categoryRepository.findAll();
		view.addObject("listCategory", listCategory);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Variants variants, BindingResult result) {
		
		if(variants.getId()!=null) {
			Variants variantslama = this.variantsRepository.findById(variants.getId()).orElse(null);
			variants.setCreateBy(variantslama.getCreateBy());
			variants.setModifyBy("user1");
			variants.setCreateDate(variantslama.getCreateDate());
			variants.setModifyDate(new Date());
//			variants.setCategory(variants.getCategory());
		} else {
			variants.setCreateBy("user1");
			variants.setCreateDate(new Date());
		}
		 if(!result.hasErrors()) {
			 this.variantsRepository.save(variants);
			 return new ModelAndView("redirect:/variants/index");
		 } else {
			 return new ModelAndView("redirect:/variants/index");
		 }
	}
	
	@GetMapping("edit/{id}")
	public ModelAndView edit (@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("variants/addformvariants");
		Variants variants = this.variantsRepository.findById(id).orElse(null);
		view.addObject("variants", variants);
		List<Category> listCategory = this.categoryRepository.findAll();
		view.addObject("listCategory", listCategory);
		return view;
		
	}
	
	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		if(id!=null) {
			this.variantsRepository.deleteById(id);
		}
		return new ModelAndView("redirect:/variants/index");
	}
}
