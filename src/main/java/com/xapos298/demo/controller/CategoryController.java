package com.xapos298.demo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos298.demo.model.Category;
import com.xapos298.demo.repository.CategoryRepository;

@Controller
@RequestMapping("/category/")
public class CategoryController {

	@Autowired
	private CategoryRepository categoryRepository;
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("category/indexapi");
		return view;
	}

	@GetMapping("index")

	public ModelAndView index() {
		ModelAndView view = new ModelAndView("category/index");
		List<Category> listCategory = this.categoryRepository.findAll();
		view.addObject("listCategory", listCategory);
		return view;
	}

	@GetMapping("addform")
	public ModelAndView addform() {
		ModelAndView view = new ModelAndView("category/addform");
		Category category = new Category();
		view.addObject("category", category);
		return view;
	}

	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Category category, BindingResult result) {
//		System.out.println(category.getCategoryName());  //(untuk cek apakah inputan sudah masuk console atau belum)
//		return new ModelAndView("redirect:/category/index");
//		category.setCreateBy("user1"); //inputan create by otomatis user 1
//		category.setCreateDate(new Date()); //inputan create date otomatis waktu ketika menginputkan data
		
		if (category.getId() != null) {
			Category categorylama = this.categoryRepository.findById(category.getId()).orElse(null);
			category.setCreateBy(categorylama.getCreateBy());
			category.setModifyBy("user1");
			category.setCreateDate(categorylama.getCreateDate());
			category.setModifyDate(new Date());
		} else {
			category.setCreateBy("user1");
			category.setCreateDate(new Date());
		}
		if (!result.hasErrors()) {
			this.categoryRepository.save(category);
			return new ModelAndView("redirect:/category/index"); // save sama seperti INSERT INTO .... VALUES ....
		} else {
			return new ModelAndView("redirect:/category/index");
		}

	}

	@GetMapping("edit/{ids}")
	public ModelAndView edit(@PathVariable("ids") Long id) { // @PathVariable untuk ambil item dari url
		ModelAndView view = new ModelAndView("category/addform");

		Category category = this.categoryRepository.findById(id).orElse(null);// SELECT * FROM CATEGORY WHERE = {id}

		view.addObject("category", category);
		return view;
	}
	
	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		if(id!=null) {
			this.categoryRepository.deleteById(id);
		}
		return new ModelAndView("redirect:/category/index");
	}
}
